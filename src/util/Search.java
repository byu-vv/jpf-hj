package util;

import gov.nasa.jpf.Config;
import gov.nasa.jpf.JPF;
import gov.nasa.jpf.PropertyListenerAdapter;
import gov.nasa.jpf.jvm.bytecode.JVMInvokeInstruction;
import gov.nasa.jpf.vm.ApplicationContext;
import gov.nasa.jpf.vm.bytecode.ArrayElementInstruction;
import gov.nasa.jpf.vm.bytecode.FieldInstruction;
import gov.nasa.jpf.vm.bytecode.ReadOrWriteInstruction;
import gov.nasa.jpf.vm.ChoiceGenerator;
import gov.nasa.jpf.vm.ElementInfo;
import gov.nasa.jpf.vm.Instruction;
import gov.nasa.jpf.vm.MethodInfo;
import gov.nasa.jpf.vm.ThreadInfo;
import gov.nasa.jpf.vm.ThreadList;
import gov.nasa.jpf.vm.VM;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.*;


/**
 * Data race detector for structured parallel programs.
 * This class makes JPF enumerate all thread schedules over isolated
 * regions in a Habanero Java program. It takes a detector as an input and
 * the detector is responsible for implementing an algorithm that can detect
 * data race in the program. This class passes information about each
 * read/write and synchronization event to the detector and also tells the
 * detector when to record a snapshot of its state and when to reset its state
 * when JPF backtracks.
 */
public class Search extends PropertyListenerAdapter {
  // Method names
  private static final String THREAD_START = "java.lang.Thread.start";
  private static final String THREAD_JOIN = "java.lang.Thread.join";
  private static final String ISOLATED = "edu.rice.hj.Module2.isolated";
  private static final String START_ISOLATION = "hj.lang.Runtime.startIsolation";
  private static final String STOP_ISOLATION = "hj.lang.Runtime.stopIsolation";
  private static final String FUTURE_GET = "hj.lang.Future.get";
  private static final String FUTURE = "hj.lang.Future";
  private static final String ACTIVITY = "hj.runtime.wsh.Activity";
  private static final String SUSPENDABLE = "hj.runtime.wsh.SuspendableActivity";
  private static final String FINISH = "hj.runtime.wsh.FinishScope";
  private static final String STOP_FINISH = "hj.runtime.wsh.Activity.stopFinish";
  private static final String START_FINISH = "hj.runtime.wsh.Activity.startFinish";
  private static final int MAIN_THREAD = 1;
  private long startTime;
  boolean debug = false;
  private boolean useDPOR = false;

  // util.Detector that implements data race detection algorithm
  protected final Detector detector;

  // Stack of finish scopes for reaping tasks at finish block end
  private Stack<FinishBlock> finishBlocks = new Stack<>();

  // Map of thread refs to dense thread ids
  private Map<Integer, Integer> tids = new HashMap<>();

  // Initial detector state for reset
  private final Object initialState;

  private String benchmarkName;

  private boolean useDC = false;
  private boolean benchmarkOutput = true;

  public Search(Config conf) {
    this(conf, null, null);
  }

  public Search(Config conf, Detector detector) {
    this(conf, null, detector);
  }

  public Search(Config conf, JPF jpf) {
    this(conf, jpf, null);
  }

  // Subclass should extend this class and call super(conf, jpf, detector); with a specific detector
  public Search(Config conf, JPF jpf, Detector detector) {
    benchmarkName = conf.getString("target");
    benchmarkName = benchmarkName.contains("benchmarks.") ? benchmarkName.split("benchmarks.")[1] : benchmarkName;
    useDPOR = conf.getBoolean("dpor");
    useDC = conf.getBoolean("dc");
    debug = conf.getBoolean("debug");
    benchmarkOutput = conf.getBoolean("bechmarkoutput");
    try {
      if (detector == null) {
        this.detector = (Detector) Class.forName(conf.getString("detector")).newInstance();
      } else {
        this.detector = detector;
      }
      this.detector.setUseDC(useDC);
      this.detector.debug = debug;
    } catch (Exception e) {
      e.printStackTrace();
      throw new RuntimeException("Could not locate detector by the name \"" +
              conf.getString("detector") +
              "\". Please specify the full package and class name if the auto-finder doesn't work.");
    }
    this.detector.owner = this;
    initialState = this.detector.getImmutableState();
  }

  @Override
  public void reset() {
    finishBlocks = new Stack<>();
    tids = new HashMap<>();
    detector.resetState(initialState);
  }

  void resetState(RaceDetectorState state) {
    finishBlocks = (Stack<FinishBlock>) state.finishBlocks.clone();
    tids = new HashMap<>(state.tids);
    detector.resetState(state.toolState);
  }

  void debug(String message) {
    if (debug) {
      System.out.println(message);
      System.out.println(detector.getImmutableState().toString());
    }
  }

  // util.Search interface
  @Override
  public void searchStarted(gov.nasa.jpf.search.Search search) {
    super.searchStarted(search);
    startTime = System.currentTimeMillis();
  }

  @Override
  public void searchFinished(gov.nasa.jpf.search.Search search) {
    long millis = System.currentTimeMillis() - startTime;
    System.out.println("Time Analyzing: " + millis);
    System.out.println("HALT COUNT: " + detector.haltCount);
    if(true || benchmarkOutput) {
      try {
        File benchmarkFile = new File(String.format("benchmark-output-%s", detector.getClass().getSimpleName()));
        boolean exists = benchmarkFile.exists();
        BufferedWriter writer = new BufferedWriter(new FileWriter(benchmarkFile, true));
        if(!exists) {
            benchmarkFile.createNewFile();
            writer.append("benchmark,millis,race,dpor,debug,haltcount\n");
        }
        writer.append(String.format("%s,%s,%s,%s,%s,%s\n", benchmarkName, millis, detector.hasRace(), useDPOR, debug, detector.haltCount));
        writer.close();
      } catch (IOException e) {
        e.printStackTrace();
        System.err.println("Failed to output to benchmark file");
      }
    }
  }

  // VM interface
  @Override
  public void executeInstruction(VM vm, ThreadInfo ti, Instruction inst) {
    //scheduler for isolated
    String label;
    if (inst instanceof JVMInvokeInstruction) {
      MethodInfo mi = ((JVMInvokeInstruction) inst).getInvokedMethod();
      String mname = mi.getBaseName();
      if (mname.equals(ISOLATED)) {
        ChoiceGenerator<ThreadInfo> cg = getRunnableCG("ISOLATED", ti, vm);
        vm.getSystemState().setNextChoiceGenerator(cg);
      }
    } else if ((label = getUniqueLabel(inst, ti)) != null) {
      ReadOrWriteInstruction rw = (ReadOrWriteInstruction) inst;
      int tid = getTid(ti.getThreadObjectRef());
      if (rw.isRead()) {
        debug("Before read " + label + " on " + tid);
        detector.handleRead(tid, label);
        debug("After read " + label + " on " + tid);
      } else {
        debug("Before write " + label + " on " + tid);
        detector.handleWrite(tid, label);
        debug("After write " + label + " on " + tid);
      }
    }
  }

  @Override
  public void methodEntered(VM vm, ThreadInfo ti, MethodInfo enteredMethod) {
    String mname = enteredMethod.getBaseName();
    if (mname.startsWith(STOP_ISOLATION)) {
      int tid = getTid(ti.getThreadObjectRef());
      AddableChoiceGenerator cg = (AddableChoiceGenerator) vm.getChoiceGenerator();
      debug("Before release on " + tid);
      detector.handleRelease(tid, cg);
      debug("After release on " + tid);

      //See which cg's we are dependent with
      List<AddableChoiceGenerator> activeCGs = Arrays.asList(vm.getChoiceGeneratorsOfType(AddableChoiceGenerator.class));
      ListIterator li = activeCGs.listIterator(activeCGs.size());
      while (useDPOR && li.hasPrevious()) { //go through the CG's most recent first
        AddableChoiceGenerator previousCG = (AddableChoiceGenerator) li.previous();
        if (detector.isDependent(previousCG, cg)) {
          ((AddableChoiceGenerator) previousCG).addNextChoice(ti); //add this thread to most recent of dependent CG's
        }
      }

    } else if (mname.startsWith(START_FINISH)) {
      finishBlocks.push(new FinishBlock(getTid(ti.getThis()), new ArrayList<>()));
    }
  }

  @Override
  public void objectCreated(VM vm, ThreadInfo ti, ElementInfo newObject) {
    String name = newObject.getClassInfo().getName();
    if (name.startsWith(ACTIVITY) || name.startsWith(FUTURE) || name.startsWith(SUSPENDABLE)) {
      int tid = getTid(ti.getThreadObjectRef());
      int child = getTid(newObject.getObjectRef());
      debug("Before fork of " + child + " on " + tid);
      detector.handleFork(tid, child, name.contains(FUTURE));
      debug("After fork of " + child + " on " + tid);
      if (finishBlocks.size() > 0)
        finishBlocks.peek().children.add(child);
      if (child == MAIN_THREAD)
        finishBlocks.push(new FinishBlock(MAIN_THREAD, new ArrayList<>()));
    }
  }

  @Override
  public void methodExited(VM vm, ThreadInfo ti, MethodInfo exitedMethod) {
    String mname = exitedMethod.getBaseName();
    int tid = getTid(ti.getThreadObjectRef());
    if (mname.startsWith(START_ISOLATION)) {
      debug("Before acquire on " + tid);
      detector.handleAcquire(tid);
      debug("After acquire on " + tid);
    } else if (mname.startsWith(FUTURE_GET)) {
      int child = getTid(ti.getThis());
      debug("Before join of " + child + " on " + tid);
      detector.handleJoin(tid, child, false);
      debug("After join of " + child + " on " + tid);
    } else if (mname.startsWith(STOP_FINISH)) {
      int parent = getTid(ti.getThis());
      FinishBlock block = finishBlocks.peek();
      if (parent != block.parent)
        return;
      for (int child : block.children) {
        debug("Before join of " + child + " on " + parent);
        detector.handleJoin(parent, child, true);
        debug("After join of " + child + " on " + parent);
      }
      finishBlocks.pop();
      if (finishBlocks.isEmpty()) {
        detector.handleHalt(vm.getSUTName());
      }
    }
  }

  @Override
  public String getErrorMessage() {
    return detector.error();
  }

  @Override
  public boolean check(gov.nasa.jpf.search.Search search, VM vm) {
    if (detector.hasRace()) {
        detector.handleHalt(vm.getSUTName() + "RaceDetected");
        return false;
    }
    return true;
  }

  // Helper methods
  ThreadInfo[] getTimeoutRunnables(VM vm, ApplicationContext appCtx) {
    ThreadList tlist = vm.getThreadList();
    if (tlist.hasProcessTimeoutRunnables(appCtx)) {
      return tlist.getProcessTimeoutRunnables(appCtx);
    } else {
      return tlist.getTimeoutRunnables();
    }
  }

  ChoiceGenerator<ThreadInfo> getRunnableCG(String id, ThreadInfo tiCurrent, VM vm) {
    ThreadInfo[] timeoutRunnables = getTimeoutRunnables(vm, tiCurrent.getApplicationContext());
    if (timeoutRunnables.length == 0) {
      throw new RuntimeException("I didn't think this happened. timoutRunnables length was 0.");
      //return null;
    }
    if (!Arrays.asList(timeoutRunnables).contains(tiCurrent)) {
      throw new RuntimeException("Current thread was not runnable");
      //return null;
    }
    AddableChoiceGenerator newCG = new AddableChoiceGenerator(id, timeoutRunnables, this, new RaceDetectorState(finishBlocks, tids, detector.getImmutableState()), tiCurrent);
    if (!useDPOR) newCG.addAllRunnables(); //start empty if using DPOR otherwise start with all possible choices
    return newCG;
  }

  int getTid(int threadRef) {
    if (!tids.containsKey(threadRef)) {
      tids.put(threadRef, tids.size());
    }
    return tids.get(threadRef);
  }

  String getUniqueLabel(Instruction insn, ThreadInfo ti) {
    if (!isValidArrayElementInstruction(insn, ti) && !isValidFieldInstruction(insn)) {
      return null;
    }
    if (insn instanceof ArrayElementInstruction) {
      ArrayElementInstruction ainsn = ((ArrayElementInstruction) insn);
      int objRef = ainsn.peekArrayElementInfo(ti).getObjectRef();
      int index = ainsn.peekIndex(ti);
      return "util.array@" + objRef + "[" + index + "]";
    } else if (insn instanceof FieldInstruction) {
      FieldInstruction finsn = ((FieldInstruction) insn);
      ElementInfo ei = finsn.getElementInfo(ti);
      return finsn.getId(ei);
    }
    return null;
  }

  boolean isValidArrayElementInstruction(Instruction inst, ThreadInfo ti) {
    if (!(inst instanceof ArrayElementInstruction)) {
      return false;
    }
    ElementInfo ei = ((ArrayElementInstruction) inst).peekArrayElementInfo(ti);
    String klass = ei.getClassInfo().getName();
    if (!klass.startsWith("[Ljava") && !klass.startsWith("[Ledu")) {
      return true;
    }
    return false;
  }

  boolean isValidFieldInstruction(Instruction insn) {
    return insn instanceof FieldInstruction && !isLibraryInstruction(insn);
  }

  boolean isLibraryInstruction(Instruction insn) {
    String className = ((FieldInstruction) insn).getClassName();
    return className.startsWith("java") || className.startsWith("hj") ||
            (className.startsWith("edu") &&
                    !(className.startsWith("edu.rice.hj.api.HjActor") ||
                            className.startsWith("edu.rice.hj.api.HjDataDrivenFuture") ||
                            className.startsWith("edu.rice.hj.api.HjFinishAccumulator") ||
                            className.startsWith("edu.rice.hj.api.HjFuture") ||
                            className.startsWith("edu.rice.hj.api.HjLambda") ||
                            className.startsWith("edu.rice.hj.api.HjRunnable") ||
                            className.startsWith("edu.rice.hj.api.HjSuspendable") ||
                            className.startsWith("edu.rice.hj.api.HjSuspendingCallable")));
  }

  String shortClassName(String name) {
    String[] s = name.split("\\.");
    return s[s.length - 1];
  }

  private static class FinishBlock {
    final int parent;
    final List<Integer> children;

    FinishBlock(int parent, List<Integer> children) {
      this.parent = parent;
      this.children = children;
    }
  }

  static class RaceDetectorState {
    final Stack<FinishBlock> finishBlocks;
    final Map<Integer, Integer> tids;
    final Object toolState;

    RaceDetectorState(Stack<FinishBlock> finishBlocks, Map<Integer, Integer> tids, Object toolState) {
      this.finishBlocks = (Stack<FinishBlock>) finishBlocks.clone();
      this.tids = new HashMap<>(tids);
      this.toolState = toolState;
    }
  }

}
