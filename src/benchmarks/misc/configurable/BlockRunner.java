package benchmarks.misc.configurable;

import java.lang.*;

import static edu.rice.hj.Module0.finish;
import static edu.rice.hj.Module0.launchHabaneroApp;
import static edu.rice.hj.Module1.async;
import static edu.rice.hj.Module2.isolated;

class BlockRunner {
  public static void run(Block block) {
    launchHabaneroApp(() -> BlockRunner.runRecursive(block));
  }

  private static void runRecursive(Block block) {
    //System.out.println("\nThread " + Thread.currentThread().getId() + " is Running Block " + block);
    for(Block statement : block) {
      if(statement instanceof Access && ((Access) statement).write) {
        System.out.print("Thread " + Thread.currentThread().getId() + " is writing " + ((Access) statement).object);
        ((Access) statement).object.value++;
        System.out.println("\t it is now " + ((Access) statement).object.value);
      } else if(statement instanceof Access && !((Access) statement).write) {
        System.out.print("Reading " + ((Access) statement).object.toString());
        char c = ((Access) statement).object.value;
        System.out.println("\t it was " + c);
      } else if(statement instanceof Synchronization && ((Synchronization) statement).async) {
        System.out.println("Launching new thread");
        async(() -> BlockRunner.runRecursive(statement));
      } else if(statement instanceof Synchronization && !((Synchronization) statement).async) {
        System.out.println("Starting new finish");
        finish(() -> BlockRunner.runRecursive(statement));
      } else if(statement instanceof Isolation) {
        System.out.println("Starting new isolated");
        isolated(() -> BlockRunner.runRecursive(statement));
      } else {
        BlockRunner.runRecursive(statement);
      }
    }
  }
}
