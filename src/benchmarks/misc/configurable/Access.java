package benchmarks.misc.configurable;

public class Access extends Block {
  public boolean write = false;
  public DummyObject object;

  public Access(boolean write, Access access) {
    this(write, access.object);
  }

  public Access(boolean write, DummyObject object) {
    this.write = write;
    this.object = object;
  }

  public Access(boolean write) {
    this.write = write;
    this.object = new DummyObject();
  }

  public Access(DummyObject object) {
    this.object = object;
  }

  public Access() {
    object = new DummyObject();
  }

  public static void resetVariableId() {
    DummyObject.count = 0;
  }

  public static class DummyObject {
    public static int count = 0;
    public char value;
    public int id;

    public DummyObject() {
        this.value = 0;
        this.id = count++;
    }

    public String toString() {
        return "v" + id;
    }

  }

  public String toString() {
    String objectLabel = object.toString();
    if (objectLabel.length() <= 12) return "[" + objectLabel + "]";
    return "[" + objectLabel.substring(objectLabel.length() - 12) + "]";
  }
}
