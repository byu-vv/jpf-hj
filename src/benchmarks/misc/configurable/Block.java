package benchmarks.misc.configurable;

import java.util.ArrayList;

public class Block extends ArrayList<Block> {

  public static Block create() {
    return new Block();
  }

  public Block read() {
    return access(false);
  }

  public Block read(Access access) {
    return access(false, access);
  }

  public Block write() {
    return access(true);
  }

  public Block write(Access access) {
    return access(true, access);
  }

  private Block access(boolean write, Access access) {
    add(new Access(write, access));
    return this;
  }

  private Block access(boolean write) {
    add(new Access(write));
    return this;
  }

  public Block multiRead(int numAcc) {
    return multiAccess(false, numAcc);
  }

  public Block multiRead(int numAcc, MultiAccess access) {
    return multiAccess(false, numAcc, access);
  }

  public Block multiWrite(int numAcc) {
    return multiAccess(true, numAcc);
  }

  public Block multiWrite(int numAcc, MultiAccess access) {
    return multiAccess(true, numAcc, access);
  }

  private Block multiAccess(boolean write, int numAcc, MultiAccess access) {
    add(new MultiAccess(write, numAcc, access));
    return this;
  }

  private Block multiAccess(boolean write, int numAcc) {
    add(new MultiAccess(write, numAcc));
    return this;
  }


  public Access get() {
    if(last() instanceof Access) {
      return (Access) last();
    } else {
      throw new RuntimeException("Method get() must only be called on an Access object");
    }
  }

  public Block last() {
    return get(size() - 1);
  }

  public Block async(Block block) {
    add(new Synchronization(block, true));
    return this;
  }

  public Block finish(Block block) {
    add(new Synchronization(block, false));
    return this;
  }

  public Block isolated(Block block) {
    add(new Isolation(block));
    return this;
  }

  public void emmit(String name) {
    BlockEmmitter.emmit(this, name);
  }

  public void run() {
    BlockRunner.run(this);
  }
}
