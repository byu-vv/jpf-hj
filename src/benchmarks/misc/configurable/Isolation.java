package benchmarks.misc.configurable;

class Isolation extends Block {
  private Isolation() { }

  public Isolation(Block block) {
    this.add(block);
  }

  public String toString() {
    return "Isolation"+super.toString();
  }
}
