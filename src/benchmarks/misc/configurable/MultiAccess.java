package benchmarks.misc.configurable;

public class MultiAccess extends Access {
  public int numAcc = 0;

  public MultiAccess(boolean write, int numAcc) {
    super(write);
    this.numAcc = numAcc;
  }

  public MultiAccess(boolean write, int numAcc, MultiAccess access) {
    super(write, access);
    this.numAcc = numAcc;
  }

}
