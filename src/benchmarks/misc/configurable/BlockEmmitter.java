package benchmarks.misc.configurable;

import java.util.*;
import java.io.*;

class BlockEmmitter {
//  private static Set<Access.DummyObject> tracker = new HashSet<>();
  static Set<String> variablesToDeclare;
  static String path = "/home/kylona/workspace/jpf-hj/src/benchmarks/generated/";
  static String name = "DefaultName";
  static PrintWriter pw = null;

  public static void emmit(Block block, String path, String name) {
    BlockEmmitter.path = path;
    emmit(block, name);
  }

  public static void emmit(Block block, String name) {
    BlockEmmitter.name = name;
    pw = null;
    variablesToDeclare = new HashSet();
    //emmit preamble
    output("package benchmarks.generated;\n", 0);
    output("import static permission.PermissionChecks.*;", 0);
    output("import static edu.rice.hj.Module2.launchHabaneroApp;", 0);
    output("import static edu.rice.hj.Module2.async;", 0);
    output("import static edu.rice.hj.Module2.finish;", 0);
    output("import static edu.rice.hj.Module2.isolated;", 0);
    output("import edu.rice.hj.api.*;", 0);

    output("public class " + name + " {", 0);
        output("public static void main(String[] args) throws SuspendableException {", 1);
            output(name + " toRun = new " + name + "();",2);
            output("toRun.run();",1);
        output("}", 0);
        output("public void run() {", 0); 
            output("launchHabaneroApp(new HjSuspendable() {", 2);
                output("public void run() {", 3);
                    emmitRecursive(block, 4);
                output("}", 3);
            output("});", 2);
        output("}", 1);
        outputVariableDeclarations(1);
    output("}", 0);
    if (pw != null) pw.close();
  }

  private static void emmitRecursive(Block block, int depth) {
    for(Block statement : block) {

      if(statement instanceof MultiAccess && ((MultiAccess) statement).write && ((MultiAccess) statement).numAcc > 1) {
        output("for(int j = 0; j < " + ((MultiAccess) statement).numAcc + "; j++) {", depth);
            output(((MultiAccess) statement).object + "++;", depth+1);
        output("}", depth);
        variablesToDeclare.add(((MultiAccess) statement).object.toString());
      } else if(statement instanceof MultiAccess && !((MultiAccess) statement).write && ((MultiAccess) statement).numAcc > 1) {
        output("for(int j = 0; j < " + ((MultiAccess) statement).numAcc + "; j++) {", depth);
            output("if("+ ((MultiAccess) statement).object + " == 0) {", depth+1);
            output("int i = 0;", depth + 2);
            output("}", depth+1);
        output("}", depth);
        variablesToDeclare.add(((MultiAccess) statement).object.toString());
      } else if(statement instanceof Access && ((Access) statement).write) {
        output(((Access) statement).object + "++;", depth);
        variablesToDeclare.add(((Access) statement).object.toString());

      } else if(statement instanceof Access && !((Access) statement).write) {
            output("if("+ ((Access) statement).object + " == 0) {", depth+1);
            output("int i = 0;", depth + 2);
            output("}", depth+1);
        variablesToDeclare.add(((Access) statement).object.toString());
      } else if(statement instanceof Synchronization && ((Synchronization) statement).async) {
        output("async( new HjRunnable() {", depth);
            output("public void run() {", depth + 1);
                emmitRecursive(statement, depth + 2);
            output("}", depth + 1);
        output("});", depth);
      } else if(statement instanceof Synchronization && !((Synchronization) statement).async) {
        output("finish( new HjRunnable() {", depth);
            output("public void run() {", depth + 1);
                emmitRecursive(statement, depth + 2);
            output("}", depth + 1);
        output("});", depth);
      } else if(statement instanceof Isolation) {
        output("isolated( new HjRunnable() {", depth);
            output("public void run() {", depth + 1);
                emmitRecursive(statement, depth + 2);
            output("}", depth + 1);
        output("});", depth);
      } else {
            emmitRecursive(statement, depth);
      }
    }
  }

  private static void outputVariableDeclarations(int depth) {
      for (String name : variablesToDeclare) {
        output("char " + name + ";", depth);
      }
  }


  private static void output(String s, int depth) {
    try {
        if (pw == null) {
			File file = new File(path + name + ".java");
			file.getParentFile().mkdirs();
			file.createNewFile();
			pw = new PrintWriter(new FileWriter(file));
        }
        for (int d = 0; d < depth; d++) {
            pw.print("  ");
        }
        pw.println(s);
    }
    catch (Exception e) {
        throw new RuntimeException(e.toString());
    }
  }
}
