package benchmarks.nonisolated;

import benchmarks.misc.configurable.Block;

public class ConfigurableSimpleYes {
  public static void main(String[] args) {
    Block thread = Block.create().write();
    Block main = Block.create().async(thread);
    main.write(thread.get()).run();
  }
}
