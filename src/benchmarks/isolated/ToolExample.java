package benchmarks.isolated;

import edu.rice.hj.api.SuspendableException;

import static edu.rice.hj.Module2.launchHabaneroApp;
import static edu.rice.hj.Module2.forAll;
import edu.rice.hj.api.*;

import static edu.rice.hj.Module1.*;
import static edu.rice.hj.Module2.isolated;

import edu.rice.hj.api.HjRunnable;
import edu.rice.hj.api.HjSuspendable;
import edu.rice.hj.api.HjFuture;


public class ToolExample {
    static Integer x = 0;

    public static void main(String[] args) throws SuspendableException
    {
        launchHabaneroApp(new HjSuspendable() {
            public void run() throws SuspendableException {
                finish(new HjSuspendable() {
                    final HjFuture<Integer> fut1 = future( () -> {
                        isolated(new HjRunnable() {
                            public void run() { x++; }
                        });
                        return x++;
                    });
                    public void run() {
                        async(new HjRunnable() {
                            public void run() {
                                isolated(new HjRunnable() {
                                    public void run() { x++; }
                                });
                                x = fut1.get();
                            }
                        });

                    }
                });
            }
        });
    }


}
