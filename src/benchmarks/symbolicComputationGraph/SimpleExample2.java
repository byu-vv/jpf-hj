package benchmarks.symbolicComputationGraph;

import edu.rice.hj.api.HjRunnable;
import edu.rice.hj.api.HjSuspendable;
import edu.rice.hj.api.SuspendableException;

import static edu.rice.hj.Module0.finish;
import static edu.rice.hj.Module1.async;
import static edu.rice.hj.Module1.launchHabaneroApp;
import static edu.rice.hj.Module2.isolated;

public class SimpleExample2 {

    static int a = 1;
    static int b = 2;
    static int c = 3;

    public static void main(String args[]) {
        launchHabaneroApp(new HjSuspendable() {
            @Override
            public void run() throws SuspendableException {
                finish(new HjSuspendable() {
                    @Override
                    public void run() throws SuspendableException {
                        async(new HjRunnable() {
                            @Override
                            public void run() {
                                a = a + 1;
                                if(c > b) {
                                    b = c + 1;
                                }
                                else if (a >= 2) {
                                    async(new HjRunnable() {
                                        @Override
                                        public void run() {
                                            b = a;
                                        }
                                    });
                                }
                                a = c;
                                async(new HjRunnable() {
                                    @Override
                                    public void run() {
                                        c = c + 1;
                                    }
                                });
                            }
                        });
                    }
                });
                b = b + 1;
                if(b > c) {
                    c = b + 1;
                }
                else {
                    b = c + 1;
                }
            }
        });
    }
}
