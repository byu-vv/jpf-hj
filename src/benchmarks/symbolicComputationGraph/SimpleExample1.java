package benchmarks.symbolicComputationGraph;

import edu.rice.hj.api.HjRunnable;
import edu.rice.hj.api.HjSuspendable;
import edu.rice.hj.api.SuspendableException;

import static edu.rice.hj.Module0.finish;
import static edu.rice.hj.Module1.async;
import static edu.rice.hj.Module1.launchHabaneroApp;
import static edu.rice.hj.Module2.isolated;

public class SimpleExample1 {

    static int a = 1;
    static int b = 2;
    static int c = 3;

    public static void main(String args[]) {

        launchHabaneroApp(new HjSuspendable() {
            public void run() {
                if(a < b) {
                    finish(new HjSuspendable() {
                        @Override
                        public void run() throws SuspendableException {
                            async(new HjRunnable() {
                                @Override
                                public void run() {
                                    a = a - 1;
                                    b = a + 1;
                                }
                            });
                            c = c + 1;
                        }
                    });
                }
                else {
                    a = a + 1;
                    b = b - 1;
                }
                int x = a + b + c;
            }
        });

    }
}
