package fasttrack;

import util.Search;
import gov.nasa.jpf.JPF;
import gov.nasa.jpf.Config;

public class FastTrackRaceDetector extends Search {
  public FastTrackRaceDetector(Config conf, JPF jpf) {
    super(conf, jpf, new FastTrackDetector());
  }
}
