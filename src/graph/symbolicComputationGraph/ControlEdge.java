package graph.symbolicComputationGraph;

public class ControlEdge extends Edge {

    private boolean condition;

    public boolean isCondition() {
        return condition;
    }

    public void setCondition(boolean condition) {
        this.condition = condition;
    }


}
