package graph.symbolicComputationGraph;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Comparator;

public class InitializeGraph {

    private ArrayList<File> files;

    public static void main(String args[]){
        //args should contain all .class files PATHS relevant to a given benchmark -- working under this assumption currently
        //OR it should just contain the class name, and we can load all relevant .class files programatically

        InitializeGraph ig = new InitializeGraph();
        ig.run(args);
    }

    public void run(String args[]) {
        for (String s : args) {
            this.files.add(new File(s));
        }
        files.sort(new Comparator<File>() {
            @Override
            public int compare(File file1, File file2) {
                return file1.getName().compareTo(file2.getName());
            }
        });
    }

    //COULD BE VERY BAD, HAVE TO REMEMBER TO CLOSE THE STREAM AFTER READING
    public FileInputStream openFile(File file){
        FileInputStream fis = null;
        try {
            fis = new FileInputStream(file);
        }
        catch (FileNotFoundException e){
            System.out.println(e.toString());
        }
        return fis;
    }


}
