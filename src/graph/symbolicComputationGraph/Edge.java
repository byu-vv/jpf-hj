package graph.symbolicComputationGraph;

public class Edge {
    private Node to;
    private Node from;

    public Node getTo() {
        return to;
    }

    public void setTo(Node to) {
        this.to = to;
    }

    public Node getFrom() {
        return from;
    }

    public void setFrom(Node from) {
        this.from = from;
    }

}
