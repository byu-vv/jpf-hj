package graph.espfutures;

import util.AddableChoiceGenerator;
import util.Detector;

import java.util.Set;
import java.util.Map;
import java.util.HashMap;
import java.util.HashSet;
import java.lang.NullPointerException;

public class ESPFuturesDetector extends Detector {

    private static class TaskState {
        // Each of these belong with a particular task, and are a part of the futures algorithm
        private Integer pre, post; //orderings based on visitation of nodes
        private Set<Integer> nonTreeEdges = new HashSet<>(); //Set of incoming nonTreeEdges--TODO, figure out where this should be, algorithm says here however
        private Integer lsa = null; //least significant ancestor
        private Integer parent = null;
        private boolean isFuture = false;
    }

    private static class ShadowSpace {
        Integer writer;
        Set<Integer> readers;

        ShadowSpace() {
            writer = null;
            readers = new HashSet<Integer>();
        }

        @Override
        public String toString() {
            return "ShadowSpace writer =" + writer + "readers = {" + readers.toString() + "}";
        }
    }

    Map<String, ShadowSpace> shadowSpaces = new HashMap<>();
    Map<Integer, TaskState> idToState = new HashMap<>();
    int dfid = 0;
    int tmpid = Integer.MAX_VALUE;
    DisjointSet sets = new DisjointSet();
    boolean race = false;
    String err;
    Integer lastIsolated = -1;
    Integer isolatedTid = Integer.MIN_VALUE;

    @Override
    public void resetState(Object state) {
        FuturesToolState toolState = (FuturesToolState) state;
        shadowSpaces = toolState.shadowSpaces;
        idToState = toolState.idToState;
        dfid = toolState.dfid;
        tmpid = toolState.tmpid;
        sets = toolState.sets;
        race = toolState.race;
        err = toolState.err;
        lastIsolated = toolState.lastIsolated;
    }

    @Override
    public Object getImmutableState() {
        HashMap ss = new HashMap(shadowSpaces);
        HashMap is = new HashMap(idToState);
        Integer lastIso = new Integer(lastIsolated);
        DisjointSet ds = new DisjointSet(sets);
        return new FuturesToolState(ss, is, dfid, tmpid, ds, race, err, lastIso);
    }

    @Override
    public void handleFork(int parent, int child, boolean future) {
        //Algorithm 1
        if (parent == 0 && child == 1) {
            //For first thread created only "launchHabaneroApp"
            sets.makeSet(child); // tid 1 is the "main" thread
            TaskState st = new TaskState();
            st.pre = dfid++;
            st.post = tmpid--;
            idToState.put(child, st);
        } else {
            handleTaskCreation(parent, child, future);
        }
    }

    private void handleTaskCreation(int parent, int child, boolean future) {
        //Algorithm 2
        sets.makeSet(child);
        TaskState st = new TaskState();
        st.pre = dfid++;
        st.post = tmpid--;
        st.parent = parent;
        if (idToState.get(parent).nonTreeEdges.isEmpty()) {
            st.lsa = idToState.get(parent).lsa;
        } else {
            st.lsa = parent;
        }
        if (future)
            st.isFuture = true;
        idToState.put(child, st);
    }

    @Override
    public void handleJoin(int parent, int child, boolean finish) {
        //Algorithm 3
        TaskState st = idToState.get(child);
        if (st == null) {
            throw new NullPointerException("Handle Join, tid " + child + " does not exist");
        } else {
            st.post = dfid++;
            tmpid++;
        }
        if (!finish)
            handleFGet(parent, child);
        else
            merge(parent, child);
    }

    private void handleFGet(int getter, int future) {
        //Algorithm 4
        if (sets.find(getter) == sets.find(idToState.get(future).parent)) {
            merge(getter, future);
        } else {
            idToState.get(getter).nonTreeEdges.add(future);
        }
    }

    private void merge(int a, int b) {
        TaskState sta = idToState.get(a);
        TaskState stb = idToState.get(b);
        sets.union(a, b);
        sta.nonTreeEdges.addAll(stb.nonTreeEdges);
    }

    //Algorithm 8
    @Override
    public void handleWrite(int tid, String uniqueLabel) {
        if (tid == 0) return;
        ShadowSpace shadow = getShadowSpace(uniqueLabel);
        Set<Integer> readers = new HashSet<>(shadow.readers);
        for (int reader : readers) {
            if (!precede(reader, tid)) {
                reportRace("Write-read race on " + uniqueLabel + " between writer " + tid + " and reader " + reader);
            } else {
                shadow.readers.remove(reader);
            }
        }
        if (shadow.writer != null && !precede(shadow.writer, tid)) {
            reportRace("Write-write race on " + uniqueLabel + " between " + tid + " and " + shadow.writer);
        }
        shadow.writer = tid;
    }

    @Override
    public void handleRead(int tid, String uniqueLabel) {
        if (tid == 0) return;
        boolean update = false;
        ShadowSpace shadow = getShadowSpace(uniqueLabel);
        Set<Integer> readers = new HashSet<>(shadow.readers);
        for (int reader : readers) {
            if (precede(reader, tid)) {
                shadow.readers.remove(reader);
                update = true;
            } else if (idToState.get(reader).isFuture || idToState.get(tid).isFuture) {
                update = true;
            }
        }
        if (shadow.writer != null && !precede(shadow.writer, tid)) {
            reportRace("Write-read race on " + uniqueLabel + " between writer " + shadow.writer + " and reader " + tid);
        }
        if (update) {
            shadow.readers.add(tid);
        }
    }

    private void reportRace(String loc) {
        race = true;
        err = loc;
    }

    private ShadowSpace getShadowSpace(String uniqueLabel) {
        if (!shadowSpaces.containsKey(uniqueLabel)) {
            shadowSpaces.put(uniqueLabel, new ShadowSpace());
        }
        return shadowSpaces.get(uniqueLabel);
    }

    @Override
    public void handleAcquire(int tid) {
        int itid = ++isolatedTid;
        handleTaskCreation(tid, itid, true);
        if (lastIsolated != -1)
            handleFGet(tid, itid);
        lastIsolated = itid;
    }

    @Override
    public void handleRelease(int tid, AddableChoiceGenerator ag) {
        handleJoin(tid, isolatedTid, false);
    }

    boolean precede(int a, int b) {
        return visit(a, b, new HashSet<>());
    }

    boolean visit(int a, int b, Set<Integer> visited) {
        if (visited.contains(b))
            return false;
        visited.add(b);
        TaskState sa = idToState.get(sets.find(a));
        TaskState sb = idToState.get(sets.find(b));
        if (sa.pre <= sb.pre && sa.post >= sb.post)
            return true;
        if (sa.pre > sb.pre)
            return false;
        for (int future : sb.nonTreeEdges) {
            if (visit(a, future, visited))
                return true;
        }
        Integer lsa = idToState.get(b).lsa;
        while (lsa != null) {
            for (int future : idToState.get(lsa).nonTreeEdges) {
                if (visit(a, future, visited))
                    return true;
            }
            lsa = idToState.get(lsa).lsa;
        }
        return false;
    }

    public boolean hasRace() {
        return race;
    }

    public String error() {
        return race ? err : null;
    }

    private static class FuturesToolState {
        final Map<String, ShadowSpace> shadowSpaces;
        final Map<Integer, TaskState> idToState;
        final int dfid;
        final int tmpid;
        final DisjointSet sets;
        final boolean race;
        final String err;
        final Integer lastIsolated;

        FuturesToolState(Map<String, ShadowSpace> shadowSpaces, Map<Integer, TaskState> idToState, int dfid, int tmpid, DisjointSet sets, boolean race, String err, Integer lastIsolated) {
            this.shadowSpaces = new HashMap<>(shadowSpaces);
            this.idToState = new HashMap<>(idToState);
            this.dfid = dfid;
            this.tmpid = tmpid;
            this.sets = new DisjointSet(sets);
            this.race = race;
            this.err = err;
            this.lastIsolated = lastIsolated;
        }
    }

} 
