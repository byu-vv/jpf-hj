package graph.espfutures;

import util.Search;
import gov.nasa.jpf.JPF;
import gov.nasa.jpf.Config;

public class ESPFuturesRaceDetector extends Search {
  public ESPFuturesRaceDetector(Config conf, JPF jpf) {
    super(conf, jpf, new ESPFuturesDetector());
  }
}
