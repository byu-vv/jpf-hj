package graph.hb;

import util.Search;
import gov.nasa.jpf.JPF;
import gov.nasa.jpf.Config;

public class HBSearch extends Search {
  public HBSearch(Config conf, JPF jpf) {
    super(conf, jpf, new HBDetector());
  }
}
