package graph.zipper.otf.node;

import graph.util.ArrayAccess;
import graph.util.ObjectAccess;

import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

public abstract class ZipperNode {
  public Map<Integer, ZipperNode> sync;
  public ZipperNode parent = null;
  public ZipperNode child = null;
  public int tid;
  protected Map<String, ObjectAccess> objectAccesses = new HashMap<>();
  protected Map<String, ArrayAccess> arrayAccesses = new HashMap<>();
  private static int count = 0;
  public int id = count;
  public boolean error = false;

  public ZipperNode(int tid, Map<Integer, ZipperNode> sync) {
    this.tid = tid;
    this.sync = sync;
    count++;
  }

  public ZipperNode(ZipperNode toCopy) {
    this.sync = new HashMap<>(toCopy.sync);
    this.parent = toCopy.parent;
    if (toCopy.child != null) throw new RuntimeException("We do not recursivly deep copy nodes");
    this.child = null;
    this.tid = toCopy.tid;
    this.objectAccesses = new HashMap<>(toCopy.objectAccesses);
    this.arrayAccesses = new HashMap<>(toCopy.arrayAccesses);
    this.id = toCopy.id; //Do not create a new ID for the copy
  }

  public ForkNode fork(int newTid) {
    ForkNode node = new ForkNode(tid, newTid, sync);
    edge(node);
    return node;
  }

  public IsolatedNode isolate() {
    IsolatedNode node = new IsolatedNode(tid, sync);
    edge(node);
    return node;
  }

  public RegularNode regular() {
    RegularNode node = new RegularNode(tid, sync);
    edge(node);
    return node;
  }

  public RegularNode join(ZipperNode current, ZipperNode other) {
    JoinNode join = new JoinNode(tid, sync);
    current.edge(join);
    other.edge(join);
    sync.remove(other.tid);
    RegularNode node = new RegularNode(tid, sync);
    join.edge(node);
    return node;
  }

  void edge(ZipperNode child) {
    this.child = child;
    child.parent = this;
  }


  public void addAccess(String label, boolean write) {
    addAccess(objectAccesses, arrayAccesses, label, write);
  }

  public void addAccess(Map<String, ObjectAccess> objectAccesses, Map<String, ArrayAccess> arrayAccesses, String label, boolean write) {
    if (isArrayAccess(label)) {
      String key = getArrayAccessKey(label);
      int i = getArrayAccessIndex(label);
      if (arrayAccesses.containsKey(key)) {
        arrayAccesses.get(key).insert(i, write);
      } else {
        arrayAccesses.put(key, new ArrayAccess(key, i, write));
      }
    } else if (!objectAccesses.containsKey(label) || !objectAccesses.get(label).write) {
      objectAccesses.put(label, new ObjectAccess(label, write));
    }
  }

  private static boolean isArrayAccess(String label) {
    return label.startsWith("util.array@");
  }

  private static String getArrayAccessKey(String label) {
    return label.substring(0, label.indexOf('['));
  }

  private static int getArrayAccessIndex(String label) {
    return Integer.parseInt(label.substring(label.indexOf('[') + 1, label.length() - 1));
  }

  public boolean conflicts(ZipperNode node) {
    return conflicts(objectAccesses, arrayAccesses, node);
  }

  public boolean conflicts(Map<String, ObjectAccess> objectAccesses, Map<String, ArrayAccess> arrayAccesses, ZipperNode node) {
    for (String k : objectAccesses.keySet()) {
      if (node.objectAccesses.containsKey(k) && objectAccesses.get(k).conflicts(node.objectAccesses.get(k))) {
        System.err.println("Found race on variable " + k);
        return true;
      }
    }
    for (String k : arrayAccesses.keySet()) {
      if (node.arrayAccesses.containsKey(k) && arrayAccesses.get(k).conflicts(node.arrayAccesses.get(k))) {
        System.err.println("Found race on variable " + k);
        return true;
      }
    }
    return false;
  }

  public static boolean checkConflictingAccess(String label, boolean write, ZipperNode node) {
    if(isArrayAccess(label)) {
      String key = getArrayAccessKey(label);
      ArrayAccess access = new ArrayAccess(label, getArrayAccessIndex(label), write);
      return node.arrayAccesses.containsKey(key) && access.conflicts(node.arrayAccesses.get(key));
    } else {
      ObjectAccess access = new ObjectAccess(label, write);
      return node.objectAccesses.containsKey(label) && access.conflicts(node.objectAccesses.get(label));
    }
  }

  public boolean checkRace() {

    return checkRace(objectAccesses, arrayAccesses);
  }

  public boolean checkRace(Map<String, ObjectAccess> objectAccesses, Map<String, ArrayAccess> arrayAccesses) {
    throw new RuntimeException("Method checkRace() should not be called on a " + this.getClass());
  }

  @Override
  public String toString() {
    return id + " (" + tid + ")";
  }
}
