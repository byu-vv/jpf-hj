package graph.zipper.otf.node;

import graph.util.ArrayAccess;
import graph.util.ObjectAccess;

import java.util.Map;

public class RaceyNode extends ZipperNode {
  public RaceyNode(int tid, Map<Integer, ZipperNode> sync) {
    super(tid, sync);
  }

  public RaceyNode(RaceyNode toCopy) {
    super(toCopy);
  }

  @Override
  public boolean checkRace(Map<String, ObjectAccess> objectAccesses, Map<String, ArrayAccess> arrayAccesses) {
    for (Map.Entry<Integer, ZipperNode> pair : sync.entrySet()) {
      if (pair.getKey() != tid) {
        ZipperNode node = pair.getValue();
        if (!node.sync.containsKey(tid)) {
          node.sync.put(tid, this);
        }
        if (checkRace(node, objectAccesses, arrayAccesses)) {
          return true;
        }
      }
    }
    return false;
  }

  private boolean checkRace(ZipperNode node, Map<String, ObjectAccess> objectAccesses, Map<String, ArrayAccess> arrayAccesses) {
    if (node == null) {
      return false;
    } else {
      boolean bothIsolated = this instanceof IsolatedNode && node instanceof IsolatedNode;
      if (!bothIsolated && conflicts(objectAccesses, arrayAccesses, node)) {
        //System.err.println(String.format("Race detected between node %s and %s.", this, node));
        node.error = true;
        this.error = true;
        return true;
      } else {
        return checkRace(node.child, objectAccesses, arrayAccesses);
      }
    }
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append(super.toString());
    sb.append(" ");
    sb.append(objectAccesses.keySet().toString());
    sb.append(arrayAccesses.keySet().toString());
    return sb.toString();
  }
}
