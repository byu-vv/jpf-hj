package graph.zipper.otf.node;

import java.util.Map;

public class RegularNode extends RaceyNode {
  public RegularNode(int tid, Map<Integer,ZipperNode> sync) {
    super(tid, sync);
  }

  public RegularNode(RegularNode toCopy) {
    super(toCopy);
  }


}
