package graph.zipper.otf;

import graph.zipper.otf.node.*;

import java.util.*;

public class ZipperOTF {
  private Map<Integer, ZipperNode> currentNodes = new HashMap<>();
  private List<IsolatedNode> isos = new ArrayList<>();
  private ZipperNode root;
  private boolean race = false;
  public boolean useDC;

  public ZipperOTF(int rootTid) {
    root = new RegularNode(rootTid, new HashMap<>());
    currentNodes.put(rootTid, root);
  }

  public ZipperOTF(ZipperOTF toCopy) {
    this.root = toCopy.root;
  }

  public void isolateStart(int tid) {
    ZipperNode current = currentNodes.get(tid);
    currentNodes.put(tid, current.isolate());
  }

  public IsolatedNode isolateEnd(int tid) {
    IsolatedNode current = (IsolatedNode) currentNodes.get(tid);
    ZipperNode parent = current.parent;
    race |= parent.checkRace();
    race |= current.checkRace();
    currentNodes.put(tid, current.regular());
    isos.add(0, current);
    return current;
  }

  public void fork(int tid, int otherTid) {
    ZipperNode current = currentNodes.get(tid);
    race |= current.checkRace();
    ForkNode fork = current.fork(otherTid);
    currentNodes.put(tid, fork.child);
    currentNodes.put(otherTid, fork.forked);
  }

  public void join(int tid, int otherTid) {
    ZipperNode current = currentNodes.get(tid);
    race |= currentNodes.get(otherTid).checkRace();
    current.sync.remove(otherTid);
    race |= current.checkRace();
    currentNodes.put(tid, current.join(current, currentNodes.get(otherTid)));
  }

  private void handleAccess(int tid, String label, boolean write) {
    ZipperNode current = currentNodes.get(tid);
    if (current instanceof IsolatedNode) {
      for (IsolatedNode node : isos) {
        if(current.tid == node.tid || current.sync.get(node.tid) == null || current.sync.get(node.tid).id > node.id) {
          continue;
        }
        if (ZipperNode.checkConflictingAccess(label, write, node)) {
          ((IsolatedNode) current).isolatedEdge(node);
        }
      }
      if(useDC && ((IsolatedNode) current).incoming.isEmpty()){
          current.parent.addAccess(label, write);
      } else {
          current.addAccess(label, write);
      }
    } else {
      current.addAccess(label, write);
    }
  }


  public void read(int tid, String label) {
    handleAccess(tid, label, false);
  }

  public void write(int tid, String label) {
    handleAccess(tid, label, true);
  }

  public void halt(String programName) {
    DotWriter.createDot(root, programName);
  }

  public boolean hasRace() {
    return race;
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("currentNodes:\n" + currentNodes);
    sb.append("isos:\n" + isos);
    sb.append("root:\n" + root);
    sb.append("race:\n" + race);
    sb.append("useDc:\n" + useDC);
    return sb.toString();
  }

  public ZipperOTF deepCopy() {
    ZipperOTF copy = new ZipperOTF(root.tid);     
    Map<Integer, ZipperNode> currentNodesCopy = new HashMap(this.currentNodes);
    List<IsolatedNode> isosCopy = new ArrayList(this.isos);
    copy.root = this.root;
    copy.race = this.race;
    copy.useDC = this.useDC;
    /*
    for (Map.Entry<Integer,ZipperNode> entry : this.currentNodes.entrySet()) {
        if (entry.getValue() instanceof RegularNode) {
            ZipperNode nodeCopy = new RegularNode((RegularNode) entry.getValue());
            currentNodesCopy.put(entry.getKey(), nodeCopy);
            if (entry.getValue() == root) {
                copy.root = nodeCopy;
            }
        }
        else {
            throw new RuntimeException("Got unexpeted Node type in current");
        }
    }
    */
    //for (IsolatedNode iso : isos) {
    //    isosCopy.add(isos.indexOf(iso), new IsolatedNode(iso));
    //}
    copy.currentNodes = currentNodesCopy;
    copy.isos = isosCopy;
    return copy;
  }

  public static ZipperOTF resetTo(ZipperOTF state) {
	ZipperOTF copy = ((ZipperOTF) state).deepCopy();
	for (ZipperNode node : copy.currentNodes.values()) {
		node.child = null;
	}
	return copy;
  }  
}
