package graph.zipper;

import graph.hb.HBDetector;
import graph.util.Graph;
import graph.util.Node;
import org.jgrapht.Graphs;

import java.util.HashMap;
import java.util.Map;
import java.util.ArrayList;
import java.util.List;

public class ZipperDetector extends HBDetector {
  List<Node> isolationOrder = new ArrayList<>(); //used for Zipper

  public ZipperDetector() {
    super();
  }

  @Override 
  public void resetState(Object state) {
    ZipperToolState toolState = (ZipperToolState)state;
    graph = toolState.graph;
    isolatedNode = toolState.isolatedNode;
    isolationOrder = toolState.isolationOrder;
    currentNodes = toolState.currentNodes;
    tasks = toolState.tasks;
  }

  @Override
  public Object getImmutableState() {
    return new ZipperToolState(graph, isolatedNode, isolationOrder, currentNodes, tasks);
  }

  @Override
  public void handleAcquire(int tid) {
    Node isoNode = Node.mkIsolatedNode();
    graph.addVertex(isoNode);
    graph.addContinuationEdge(currentNodes.get(tid), isoNode);
    currentNodes.put(tid, isoNode);
    isolationOrder.add(isoNode);
    if (isolatedNode != null)
      graph.addIsolatedEdge(isolatedNode, isoNode);
    isolatedNode = isoNode;
  }

  public List<Node> getIsolationOrder() {
    return isolationOrder;
  }

  class ZipperToolState {
    final Graph graph;
    final Node isolatedNode;
    final Map<Integer, Node> currentNodes;
    final List<Node> isolationOrder; //used for graph.zipper
    final int tasks;
    ZipperToolState(Graph graph, Node isolatedNode, List<Node> isolationOrder, Map<Integer, Node> currentNodes, int tasks) {
      this.graph = new Graph();
      Graphs.addGraph(this.graph, graph);
      this.isolatedNode = isolatedNode;
      this.isolationOrder = isolationOrder;
      this.currentNodes = new HashMap<>(currentNodes);
      this.tasks = tasks;
    }

    @Override
    public String toString() {
      return "";
    }
  }
}
