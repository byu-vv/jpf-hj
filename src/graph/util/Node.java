package graph.util;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class Node {
  public static enum NodeType { ASYNC, JOIN, ACTIVITY, ISOLATED }
  static int count = 0;

  NodeType type;
  Map<String, ObjectAccess> objAccesses = new HashMap<>();
  Map<String, ArrayAccess> arrAccesses = new HashMap<>();
  int index;
  boolean hasIncomingIsolationEdge = false;
  boolean hasOutgoingIsolationEdge = false;
  boolean readyForJoin = false;
  public boolean error = false;

  private Node(NodeType type) {
    this.type = type;
    this.index = count++;
  }

  public static Node mkForkNode() {
    return new Node(NodeType.ASYNC);
  }

  public static Node mkJoinNode() {
    return new Node(NodeType.JOIN);
  }

  public static Node mkActivityNode() {
    return new Node(NodeType.ACTIVITY);
  }

  public static Node mkIsolatedNode() {
    return new Node(NodeType.ISOLATED);
  }

  private String arrLabel(String label) {
    return label.substring(0, label.indexOf('['));
  }

  private int arrIndex(String label) {
    int i = label.indexOf('[');
    return Integer.parseInt(label.substring(i + 1, label.length() - 1));
  }

  public void addAccess(String label, boolean write) {
    if (label.startsWith("util.array@")) {
      String k = arrLabel(label);
      int i = arrIndex(label);
      if (arrAccesses.containsKey(k)) {
        arrAccesses.get(k).insert(i, write);
      } else
        arrAccesses.put(k, new ArrayAccess(k, i, write));
    } else if (!objAccesses.containsKey(label) || !objAccesses.get(label).write)
      objAccesses.put(label, new ObjectAccess(label, write));
  }

  public boolean isJoin() {
    return type == NodeType.JOIN;
  }

  public boolean isReadWrite() {
    return objAccesses.size() > 0 || arrAccesses.size() > 0;
  }

  public boolean isAsync() {
    return type == NodeType.ASYNC;
  }

  public boolean isIsolated() {
    return type == NodeType.ISOLATED;
  }

  public int getIndex() {
    return index;
  }

  public void setHasOutgoingIsolationEdge(boolean b) {
    hasOutgoingIsolationEdge = b;
  }

  public void setHasIncomingIsolationEdge(boolean b) {
    hasIncomingIsolationEdge = b;
  }

  public boolean hasOutgoingIsolationEdge() {
    return hasOutgoingIsolationEdge;
  }

  public boolean hasIncomingIsolationEdge() {
    return hasIncomingIsolationEdge;
  }

  public Set<String> intersection(Node n) {
    Set<String> intersection = new HashSet<>();
    // objects
    for (String k : objAccesses.keySet()) {
      if (n.objAccesses.containsKey(k) && objAccesses.get(k).conflicts(n.objAccesses.get(k)))
        intersection.add(k);
    }
    // arrays
    for (String k : arrAccesses.keySet()) {
      if (n.arrAccesses.containsKey(k) && arrAccesses.get(k).conflicts(n.arrAccesses.get(k)))
        intersection.add(k);
    }
    return intersection;
  }

  public void setReadyForJoin(boolean input) {
    this.readyForJoin = input;
  }

  public boolean isReadyForJoin() {
    return this.readyForJoin;
  }

  public void union(Node n) {
    //TODO
  }

  public void clear() {
    objAccesses.clear();
    arrAccesses.clear();
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append(index);
    sb.append('\n');
    sb.append(objAccesses.toString());
    sb.append('\n');
    sb.append(arrAccesses.toString());
    return sb.toString();
  }
}
