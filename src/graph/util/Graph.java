package graph.util;

import org.jgraph.graph.AttributeMap;
import org.jgraph.graph.DefaultEdge;
import org.jgraph.graph.GraphConstants;
import org.jgrapht.experimental.dag.DirectedAcyclicGraph;
import org.jgrapht.ext.ComponentAttributeProvider;
import org.jgrapht.ext.DOTExporter;
import org.jgrapht.ext.IntegerNameProvider;

import java.awt.*;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class Graph extends DirectedAcyclicGraph<Node, DefaultEdge> {
  public static final long serialVersionUID = -1;

  public Graph() {
    super(DefaultEdge.class);
  }

  public boolean happensBefore(Node n1, Node n2) {
    if (n1 == n2)
      return true;
    for (DefaultEdge edge : outgoingEdgesOf(n1)) {
      Node child = getEdgeTarget(edge);
      if (happensBefore(child, n2))
        return true;
    }
    return false;
  }

  public boolean isIsolatedEdge(DefaultEdge e) {
    return e.getAttributes().equals(IsolatedEdgeAttributes());
  }

  public static <V, E> AttributeMap SpawnEdgeAttributes() {
    AttributeMap map = new AttributeMap();
    GraphConstants.setLineColor(map, Color.GREEN);
    GraphConstants.setLineStyle(map, 2);
    return map;
  }

  public static <V, E> AttributeMap FutureEdgeAttributes() {
    AttributeMap map = new AttributeMap();
    GraphConstants.setLineColor(map, Color.BLUE);
    GraphConstants.setLineStyle(map, 2);
    return map;
  }

  public static <V, E> AttributeMap JoinEdgeAttributes() {
    AttributeMap map = new AttributeMap();
    GraphConstants.setLineColor(map, Color.RED);
    return map;
  }

  public static <V, E> AttributeMap IsolatedEdgeAttributes() {
    AttributeMap map = new AttributeMap();
    GraphConstants.setLineColor(map, Color.PINK);
    return map;
  }

  public static <V, E> AttributeMap ContinuationEdgeAttributes() {
    AttributeMap map = new AttributeMap();
    GraphConstants.setLineColor(map, Color.BLACK);
    return map;
  }

  public DefaultEdge addSpawnEdge(Node n1, Node n2) {
    DefaultEdge e1 = addEdge(n1, n2);
    e1.setAttributes(SpawnEdgeAttributes());
    return e1;
  }

  public DefaultEdge addFutureEdge(Node n1, Node n2) {
    DefaultEdge e1 = addEdge(n1, n2);
    e1.setAttributes(FutureEdgeAttributes());
    return e1;
  }

  public DefaultEdge addJoinEdge(Node n1, Node n2) {
    DefaultEdge e1 = addEdge(n1, n2);
    if (e1 != null)
      e1.setAttributes(JoinEdgeAttributes());
      return e1;
  }

  public DefaultEdge addContinuationEdge(Node n1, Node n2) {
    DefaultEdge e1 = addEdge(n1, n2);
    e1.setAttributes(ContinuationEdgeAttributes());
    return e1;
  }

  public DefaultEdge addIsolatedEdge(Node n1, Node n2) {
    DefaultEdge e1 = addEdge(n1, n2);
    e1.setAttributes(IsolatedEdgeAttributes());
    n1.setHasOutgoingIsolationEdge(true);
    n2.setHasIncomingIsolationEdge(true);
    return e1;
  }

  public void writeGraph(String fname) {
    IntegerNameProvider<Node> p1 = new IntegerNameProvider<Node>();
    ComponentAttributeProvider<Node> p2 = new ComponentAttributeProvider<Node>() {
      @Override
      public Map<String, String> getComponentAttributes(Node arg0) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("label", String.valueOf(arg0.getIndex()));
        if(arg0.error){
          map.put("shape","doubleoctagon");
        }
        if (arg0.isIsolated()) {
          map.put("color", "pink");
        }
        return map;
      }
    };

    ComponentAttributeProvider<DefaultEdge> p3 = new ComponentAttributeProvider<DefaultEdge>() {
      @Override
      public Map<String, String> getComponentAttributes(DefaultEdge arg0) {
        Map<String, String> map = new HashMap<String, String>();
        if (arg0.getAttributes().equals(SpawnEdgeAttributes())) {
          map.put("color", "green");
          map.put("arrowhead", "diamond");
        } else if (arg0.getAttributes().equals(FutureEdgeAttributes())) {
          map.put("color", "blue");
          map.put("arrowhead", "dot");
        } else if (arg0.getAttributes().equals(JoinEdgeAttributes())) {
          map.put("color", "red");
          map.put("arrowhead", "vee");
        } else if (arg0.getAttributes().equals(IsolatedEdgeAttributes())) {
          map.put("color", "pink");
          map.put("style", "dashed");
        } else if (arg0.getAttributes().equals(ContinuationEdgeAttributes())) {
          map.put("color", "black");
        } else {
          map.put("color", "black");
        }
        return map;
      }
    };

    DOTExporter<Node, DefaultEdge> exporter = new DOTExporter<Node, DefaultEdge>(p1, null, null, p2, p3);
    try {
      exporter.export(new FileWriter(fname), this);
    } catch (IOException e) {
      e.printStackTrace();
    }
  }
}

