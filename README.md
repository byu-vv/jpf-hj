# How to run one or all of the benchmarks:
1. Clone the repository
2. Ensure you are running JDK version 1.8.0_181.
    * Otherwise you can download and install it here:
      https://www.oracle.com/technetwork/java/javase/downloads/java-archive-javase8-2177648.html
3. On windows, run `gradlew.bat`, otherwise run `gradlew`.
    * Build the project with: `./gradlew build`
    * Typical gradle tasks will work and be needed along with build, such as `clean`
    * Simply run `./gradlew runInteractive --no-daemon --console=plain` to specify parameters interactively, or specify them as described below. (The daemon must be disabled to run an interactive script, and --console=plain disables gradle's logging)
    * Available parameters are specified in the following format: `-PparameterName=value`, for example: '-Pdetector=hb'
    * The gradle run code will find a matching class for parameters that require a class; for example 'hb' will match to
      'graph.hb.HBDetector'. You can specify the fully qualified class name or the simple class name by itself, and the script will find
      a class that matches with combinations of adding "Detector" and ".class" to the end (case insensitive)
    * The available parameters are:
        * search: This is the Search (or extending the Search class) class to use. Defaults to the Search class,
          shouldn't need to be set usually. Matches as described above.
        * detector: This is the detector you want to run; matching the name is described above. Defaults to 'hb'
        * benchmark: If specified, the benchmark to run, as matched by the process described above, if not specified,
          all benchmarks are ran.
        * dpor: If set to "true" this will switch the dpor flag on (if the detector implements dpor search, it will use
          this flag to know whether to run it or not; otherwise it will run the default search).
        * debug: If set to "true", turns the global debug flag on.
        * config: If this is set, it overrides all other parameters and specifies a config file to obtain all of the
          parameters.

#Example commands
* `./gradlew clean build`: Deletes class files and .png files (from the dots folder) and then rebuilds the classes
* `./gradlew run -Pconfig=myDetector.jpf`: Runs JPF with parameters as set in the config/myDetector.jpf file on all
  benchmarks
* `./gradlew run -Pconfig=myDetector.jpf -Pbenchmark=myBenchmark`: Runs JPF with parameters as set in the config/myDetector.jpf file on the myBenchmark class (matching as described above)
* `./gradlew run -Pdetector=myDetector`: Runs JPF using the myDetector class, matching as described above, on all
  benchmarks
* `./gradlew run -Pdetector=myDetector -Pbenchmark=myBenchmark`: Runs JPF using the myDetector class and the myBenchmark class, matching as described above
* `./gradlew run -Psearch=mySearch -Pdetector=myDetector -Pbenchmark=myBenchmark`: Runs myDetector on myBenchmark, but
  also specifies a custom, non-generic Search class to be used
* `./gradlew run -Pdetector=myDetector -Pbenchmark=myBenchmark -Pdpor=true`: Runs myDetector on myBenchmark, specifying
  to use dpor if it has been implemented (otherwise it defaults to regular search)
* `./gradlew run -Pdetector=myDetector -Pbenchmark=myBenchmark -Pdebug=true`: Runs myDetector on myBenchmark, specifying
  to turn debug on
    
# Other info
* Output any dot files to the `dots` folder, and you can use the `dotsToPngs` gradle task to build them to pngs. Also,
  clean will delete and .png files from this folder
* The benchmarks are in the benchmarks package in the src folder
* JPF config ;sfiles are in the config folder in the root
* Feel free to add any gradle tasks to `build.gradle` that you need
